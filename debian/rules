#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DH_GOLANG_GO_GENERATE := 1
#export DH_GOLANG_INSTALL_ALL := 1  # test data
#export DH_GOLANG_INSTALL_EXTRA := vendor
export DH_RUBY = --gem-install
export DH_RUBY_USE_DH_AUTO_INSTALL_DESTDIR = debian/ruby-gitaly
export GEM2DEB_TEST_RUNNER = --check-dependencies

include /usr/share/dpkg/pkg-info.mk
## Set some build variables as per Makefile:
VERSION  ?= $(shell sh -c 'printf "$${1%%+dfsg*}"' -- "$(DEB_VERSION_UPSTREAM)")
PKG := gitlab.com/gitlab-org/gitaly
GO_LDFLAGS ?= -X $(PKG)/internal/version.version=$(VERSION)

%:
	dh $@  --buildsystem=golang --with=golang --package=gitaly
	dh $@  --buildsystem=ruby --with=ruby --package=ruby-gitaly

override_dh_clean:
	dh_clean
	## Remove Files-Excluded (when built from checkout or non-DFSG tarball):
	$(RM) -rv `perl -0nE 'say $$1 if m{^Files\-Excluded\:\s*(.*?)(?:\n\n|Files:|Comment:)}sm;' debian/copyright`
	-find vendor -mindepth 1 -type d -empty -delete -printf 'removed %p\n'
	rm -rf ruby/proto/gitaly/*_pb.rb
	dh_auto_clean -O--buildsystem=golang -O--package=gitaly
	dh_auto_clean -O--buildsystem=ruby -O--package=ruby-gitaly

override_dh_auto_build:
	dh_auto_build -O--buildsystem=golang -O--package=gitaly -- \
	   -ldflags "$(GO_LDFLAGS)"
	_support/generate-proto-ruby
	dh_auto_build -O--buildsystem=ruby -O--package=ruby-gitaly

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
	-dh_auto_test -O--buildsystem=golang -O--package=gitaly
	dh_auto_test -O--buildsystem=ruby -O--package=ruby-gitaly
	# Test if ruby dependencies are satisfied
	TMPDIR=$$(mktemp -d); cp ruby/Gemfile $$TMPDIR; cd $$TMPDIR;\
        bundle install --local || exit 1;\
        rm $$TMPDIR/Gemfile*; rmdir $$TMPDIR
endif

override_dh_auto_install:
	dh_auto_install -O--buildsystem=golang -O--package=gitaly --destdir=debian/gitaly
	dh_auto_install -O--buildsystem=ruby -O--package=ruby-gitaly --destdir=debian/ruby-gitaly
	$(RM) -rv debian/gitaly/usr/share/gocode

override_dh_install:
	dh_install
	$(RM) -rv debian/gitaly/usr/share/gitaly/ruby/Gemfile.lock
	$(RM) -rv debian/gitaly/usr/share/gitaly/ruby/.gitignore
	$(RM) -rv debian/gitaly/usr/share/gitaly/ruby/gitlab-shell/.gitignore
