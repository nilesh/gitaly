gitaly (1.59.3+dfsg-1) experimental; urgency=medium

  [ Utkarsh Gupta ]
  * New upstream version 1.59.3+dfsg

  [ Pirate Praveen ]
  * Drop dh_auto_configure and move _support/generate-proto-ruby to
    override_dh_auto_build

 -- Pirate Praveen <praveen@debian.org>  Thu, 24 Oct 2019 22:46:00 +0530

gitaly (1.58.0+dfsg-2) experimental; urgency=medium

  * Binary included upload to work around buildd failure. 

 -- Pirate Praveen <praveen@debian.org>  Tue, 22 Oct 2019 18:45:54 +0530

gitaly (1.58.0+dfsg-1) experimental; urgency=medium

  * New upstream version 1.58.0+dfsg
  * Bump Standards-Version to 4.4.1 (no changes needed)
  * Generate new binary ruby-gitaly
  * Remove generated files

 -- Sruthi Chandran <srud@debian.org>  Sun, 20 Oct 2019 22:54:28 +0530

gitaly (1.53.3+debian-1) experimental; urgency=medium

  * New upstream version 1.53.3+debian

 -- Sruthi Chandran <srud@debian.org>  Mon, 30 Sep 2019 18:29:01 +0530

gitaly (1.47.3+debian-1) experimental; urgency=medium

  * New upstream version 1.47.3+debian
  * Refresh patches
  * Reintroduce vendor/ to embed dependencies

 -- Sruthi Chandran <srud@debian.org>  Fri, 27 Sep 2019 17:13:01 +0530

gitaly (1.42.5+debian-1) experimental; urgency=medium

  * New upstream version 1.42.5+debian
  * Bump Standards-Version to 4.4.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sun, 11 Aug 2019 15:19:04 +0530

gitaly (1.34.3+debian-1) experimental; urgency=medium

  * New upstream version 1.34.3+debian
  * Change logging level to warn

 -- Pirate Praveen <praveen@debian.org>  Sun, 07 Jul 2019 22:57:26 +0530

gitaly (1.34.1+debian-1) experimental; urgency=medium

  * Team upload
  * New upstream version 1.34.1+debian
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Refresh patches

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Fri, 17 May 2019 04:18:21 +0530

gitaly (1.20.0+debian-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * rules: set DH_GOLANG_GO_GENERATE.

  [ Sruthi Chandran ]
  * New upstream version 1.20.0+debian
  * Fix gitaly.tmpfile to create run/gitlab/sockets (Closes: #918070)

 -- Sruthi Chandran <srud@disroot.org>  Fri, 15 Mar 2019 13:39:24 +0530

gitaly (1.12.0+debian-1) experimental; urgency=medium

  * New upstream version 1.12.0+debian
  * Tighten dependencies
  * Use embedded protobuf and genproto

 -- Pirate Praveen <praveen@debian.org>  Sun, 23 Dec 2018 11:54:55 +0530

gitaly (0.129.0+debian-3) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 15 Dec 2018 15:01:59 +0530

gitaly (0.129.0+debian-2) experimental; urgency=medium

  * Trigger Gemfile.lock update when ruby libraries change (Closes: #909191)

 -- Pirate Praveen <praveen@debian.org>  Fri, 14 Dec 2018 17:00:55 +0530

gitaly (0.129.0+debian-1) experimental; urgency=medium

  * New upstream version 0.129.0+debian
  * Refresh patches
  * Tighten dependencies
  * Relax bundler version (version in debian is not affected by bug mentioned
    in ruby/Gemfile)

 -- Pirate Praveen <praveen@debian.org>  Thu, 13 Dec 2018 13:04:38 +0530

gitaly (0.125.1+debian-3) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 08 Dec 2018 12:30:21 +0530

gitaly (0.125.1+debian-2) experimental; urgency=medium

  * Switch to github-linguist from gitlab-linguist

 -- Pirate Praveen <praveen@debian.org>  Wed, 05 Dec 2018 23:09:44 +0530

gitaly (0.125.1+debian-1) experimental; urgency=medium

  * New upstream version 0.125.1+debian

 -- Pirate Praveen <praveen@debian.org>  Mon, 26 Nov 2018 17:25:41 +0530

gitaly (0.120.1+debian-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Thu, 22 Nov 2018 20:43:07 +0530

gitaly (0.120.1+debian-1) experimental; urgency=medium

  * New upstream version 0.120.1+debian
  * Refresh patches
  * Tighten dependency on ruby-gitaly-proto

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Nov 2018 11:58:44 +0530

gitaly (0.117.3+debian-2) unstable; urgency=medium

  * Reupload to unstable
  * Allow ruby-github-linguist 5.x (gitlab needs 5.x)

 -- Pirate Praveen <praveen@debian.org>  Tue, 20 Nov 2018 10:24:27 +0530

gitaly (0.117.3+debian-1) experimental; urgency=medium

  * New upstream version 0.117.3+debian
  * Refresh patches
  * Update dependencies

 -- Pirate Praveen <praveen@debian.org>  Mon, 19 Nov 2018 19:47:55 +0530

gitaly (0.111.4+debian-3) unstable; urgency=medium

  * Relax dependency on gitaly-proto (Closes: #914055)

 -- Pirate Praveen <praveen@debian.org>  Mon, 19 Nov 2018 12:24:45 +0530

gitaly (0.111.4+debian-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Nov 2018 12:59:23 +0530

gitaly (0.111.4+debian-1) experimental; urgency=medium

  * New upstream version 0.111.4+debian

 -- Pirate Praveen <praveen@debian.org>  Tue, 13 Nov 2018 14:18:59 +0530

gitaly (0.100.1+debian2-1) unstable; urgency=medium

  * Switch back to vendored copies of grpc and golang-gitaly-proto
    (Closes: #912229)

 -- Pirate Praveen <praveen@debian.org>  Thu, 01 Nov 2018 15:47:28 +0530

gitaly (0.100.1+debian1-1) unstable; urgency=medium

  * Use grpc and gitaly-proto from system (remove vendored copies)

 -- Pirate Praveen <praveen@debian.org>  Sat, 20 Oct 2018 23:58:02 +0530

gitaly (0.100.1+debian-1) unstable; urgency=medium

  * New upstream version 0.100.1+debian
  * Use config.toml to specify languages.json

 -- Pirate Praveen <praveen@debian.org>  Thu, 18 Oct 2018 18:21:47 +0530

gitaly (0.96.2+debian-3) unstable; urgency=medium

  * Reupload to unstable (ruby-rugged >=0.27, ruby-grpc >=1.10 in unstable now)
  * Bump Standards-Version to 4.2.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Fri, 12 Oct 2018 23:46:46 +0530

gitaly (0.96.2+debian-2) experimental; urgency=medium

  * Tighten/update dependencies to use gitlab forks
  * Make .bundle directory writably by symlinking to /var/lib/gitlab
  * Regenerate Gemfile.lock in postinst

 -- Pirate Praveen <praveen@debian.org>  Fri, 20 Jul 2018 18:31:54 +0530

gitaly (0.96.2+debian-1) experimental; urgency=medium

  * New upstream release
  * Use gitlab-common for user creation
  * Setup user created by gitlab-common for gitaly service
  * Remove dependencies provided by gitlab-common
  * Update watch file pattern
  * Use vendored version of golang-github-prometheus-client-golang-dev
    (Closes: #901223)

 -- Pirate Praveen <praveen@debian.org>  Thu, 14 Jun 2018 19:22:23 +0530

gitaly (0.96.1+debian-4) experimental; urgency=medium

  * Tighten dependencies (for stretch-backports build)

 -- Pirate Praveen <praveen@debian.org>  Mon, 04 Jun 2018 21:10:35 +0530

gitaly (0.96.1+debian-3) experimental; urgency=medium

  * Relax more ruby dependencies in Gemfile
  * Add ruby dependencies in control
  * Add a test to check ruby dependencies
  * Upload to experimental (need ruby-rugged and ruby-grpc from experimental)

 -- Pirate Praveen <praveen@debian.org>  Sat, 12 May 2018 20:00:04 +0530

gitaly (0.96.1+debian-2) unstable; urgency=medium

  * Don't install Gemfile.lock
  * Relax ruby dependencies in Gemfile
  * Bump standards version to 4.1.3

 -- Pirate Praveen <praveen@debian.org>  Thu, 03 May 2018 20:42:13 +0530

gitaly (0.96.1+debian-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Pirate Praveen ]
  * Install config.toml
  * Fix paths, install ruby code
  * New upstream patch to fix github-linguist path (Closes: #894277)

  [ Dmitry Smirnov ]
  * New upstream release.
  * rules:
    + don't install golang sources (Closes: #893680).
    + expose version string.
    + enabled tests but ignore failures.
    + dh_clean to remove Files-Excluded.
  * control: added "Built-Using" field.
  * Build-Depends: fixed debhelper version to allow building in backports.
  * Build-Depends:
    + golang-golang-x-crypto-dev
    + golang-golang-x-sync-dev
    + golang-golang-x-sys-dev
    + golang-golang-x-text-dev
    + golang-google-genproto-dev
  * Depends:
    - golang-go
    + ruby-github-linguist
    + adduser
    + pipexec
    + procps
    + "ruby | ruby-interpreter" (L: ruby-script-but-no-ruby-dep)
  * Introduced "debian/gbp.conf".
  * Introduced service files.

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Apr 2018 12:24:32 +0530

gitaly (0.91.0+debian-1) unstable; urgency=medium

  * New upstream version 0.91.0 (Closes: #882476)
  * Fix watch file to look for new download url pattern
  * Bump debhelper compat to 11 and standards version to 4.1.3
  * Keep vendored versions of google.golang.org/grpc and
    gitlab.com/gitlab-org/gitaly-proto
  * Remove dh-make templates from debian/rules
  * Install NOTICE as docs

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Mar 2018 14:47:39 +0530

gitaly (0.6.0+debian-1) unstable; urgency=medium

  * Initial release (Closes: #870606)

 -- Pirate Praveen <praveen@debian.org>  Thu, 03 Aug 2017 15:48:08 +0530
