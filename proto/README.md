# Vendored copy of gitaly-proto

Vendored from gitlab.com/gitlab-org/gitaly-proto at a1e8074ed11a176e964c505dc30d153cbe8fed4d.

Migration in progress, see
https://gitlab.com/gitlab-org/gitaly/issues/1761. Do not edit files in
this directory, your changes will be ignored and overwritten.
